using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class Vendas
    {
        [Key]
        public int VendasId { get; set; }
        public int ItemId { get; set; }
        public Itens Item { get; set; }        
        public Vendedores Vendedor { get; set; }
        public DateTime Data { get; set; }
        public string VendasStatus { get; set; }
    }

}
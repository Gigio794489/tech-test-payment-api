namespace tech_test_payment_api.Models
{
    public enum EnumStatusVendas
    {
        PagamentoAprovado,
        EnviadoParaTransportadora,
        Entregue,
        Cancelada
    }
}
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class Vendedores
    {
        [Key]
        public int VendedorId { get; set; }
        public string NomeVendedor { get; set; }
        public string CPF { get; set; }
        public string Email { get; set; }
        public  int Telefone { get; set; }
    }
}
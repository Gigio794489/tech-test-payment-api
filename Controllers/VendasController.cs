using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendasController : ControllerBase
    {
        private readonly OrganizadorContext
         _context;

        public VendasController(OrganizadorContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult Criar(Vendas venda)
        {
            _context.Add(venda);
            _context.SaveChanges();            
            return Ok(venda);
        }

        [HttpGet("ObterTodos")]
        public IActionResult ObterTodos()
        {

            var todasVendas = _context.Venda;

            if (todasVendas == null)
            {
                return NotFound();
            }
            return Ok(todasVendas);
        }

        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, Vendas venda)
        {
            var statusVenda = _context.Venda.Find(id);

            if (statusVenda == null)
                return NotFound();

            statusVenda.VendasStatus = venda.VendasStatus;

            _context.Venda.Update(statusVenda);
            _context.SaveChanges();
            return Ok(statusVenda);
        }

        [HttpDelete("{id}")]
        public IActionResult Deletar(int id)
        {
            var delVenda = _context.Venda.Find(id);

            if (delVenda == null)
                return NotFound();

            _context.Venda.Remove(delVenda);
            _context.SaveChanges();
            return NoContent();
        }

    }
}
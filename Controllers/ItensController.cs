using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ItensController : ControllerBase
    {
        private readonly OrganizadorContext _context;

        public ItensController(OrganizadorContext context)
        {
            _context = context;
        }
        
        [HttpPost]
        public IActionResult Criar(Itens item)
        {

            _context.Add(item);
            _context.SaveChanges();
            return Ok(item);
        }

        [HttpGet("ObterTodos")]
        public IActionResult ObterTodos()
        {
            
            var todosItens = _context.Item;

            if (todosItens == null)
            {
                return NotFound();
            }
            return Ok(todosItens);
        }

        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, Itens item)
        {
            var itemBanco = _context.Item.Find(id);

            if (itemBanco == null)
                return NotFound();

            itemBanco.ItemNome = item.ItemNome;
            itemBanco.PrecoItem = item.PrecoItem;

            _context.Item.Update(itemBanco);
            _context.SaveChanges();
            return Ok(itemBanco);
        }

        [HttpDelete("{id}")]
        public IActionResult Deletar(int id)
        {
            var delItem = _context.Item.Find(id);

            if (delItem == null)
                return NotFound();

            _context.Item.Remove(delItem);
            _context.SaveChanges();
            return NoContent();
        }        


    }
}